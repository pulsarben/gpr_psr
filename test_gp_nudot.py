# Simple tests for gp_nudot.py
import gp_nudot
import os
import pytest
import numpy as np
import sys

DATA_DIR = os.path.dirname(__file__) + "/fixtures"

eph = DATA_DIR + "/B1540-06.eph"
res = DATA_DIR + "/B1540-06.res"
cov = DATA_DIR + "/1d_cov.dat"

def get_object():
    '''
    Set up object for tests
    '''
    eph = DATA_DIR + "/B1540-06.eph"
    res = DATA_DIR + "/B1540-06.res"
    gp = gp_nudot.calcNudot(eph, res)
    return gp

def test_checker_eph_true():
    expected = True
    result = get_object()._check_file(eph)
    assert result == expected

def test_checker_res_true():
    expected = True
    result = get_object()._check_file(res)
    assert result == expected

def test_checker_eph_false():
    eph = DATA_DIR + "/test.eph"
    expected = False
    result = get_object()._check_file(eph)
    assert result == expected

def test_checker_res_false():
    res = DATA_DIR + "/test.res"
    expected = False
    result = get_object()._check_file(res)
    assert result == expected

def test_p_and_fdot():
    period = 0.709064418522678
    nudot = -1.7474592166086364e-15
    result = get_object()._read_eph(eph)
    assert result[0] == period
    assert result[1] == nudot
