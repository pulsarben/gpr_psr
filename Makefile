#
# Makefile for building and publishing the `pulsar variability` image.
#

# ----------------------------------------------------------------------------
# Variables
# ----------------------------------------------------------------------------
IMAGE_NAME:=pulsar_variability_tools
VERSION:=$(shell awk -F= '/.*/{print $$1}' VERSION)
IMAGE:=pulsarben/$(IMAGE_NAME)

# ----------------------------------------------------------------------------
# Docker helper targets
# ----------------------------------------------------------------------------

build:  ## Build the docker image
	docker build -t $(IMAGE):$(VERSION) .


push:   ## Push the image to the remote repository
	docker push "$(IMAGE):$(VERSION)"

